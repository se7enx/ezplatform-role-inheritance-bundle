<?php

namespace ContextualCode\EzPlatformRoleInheritanceBundle\Controller;

use ContextualCode\EzPlatformRoleInheritanceBundle\Services\RoleInheritanceService;
use eZ\Publish\API\Repository\Exceptions\UnauthorizedException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use eZ\Publish\API\Repository\RoleService;
use eZ\Publish\API\Repository\Values\User\Role;
use eZ\Publish\Core\MVC\Symfony\Security\Authorization\Attribute;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RoleInheritanceController extends Controller
{
    /**
     * @var string
     */
    protected $template = 'ContextualCodeEzPlatformRoleInheritanceBundle:themes/admin/admin/role:inheritance.html.twig';

    /** @var \eZ\Publish\API\Repository\RoleService */
    protected $roleService;

    /**
     * @var RoleInheritanceService
     */
    protected $roleInheritanceService;

    /**
     * InheritanceController constructor.
     * @param RoleService $roleService
     * @param RoleInheritanceService $roleInheritanceService
     */
    public function __construct(
        RoleService $roleService,
        RoleInheritanceService $roleInheritanceService
    ) {
        $this->roleService = $roleService;
        $this->roleInheritanceService = $roleInheritanceService;
    }

    /**
     * @param \eZ\Publish\API\Repository\Values\User\Role $role
     * @param string $routeName
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Role $role, string $routeName): Response
    {
        $roleId = $role->id;
        $parentRoles = $this->roleInheritanceService->fetchAllParentRoles($roleId);
        $childRoles = $this->roleInheritanceService->fetchAllChildRoles($roleId);

        try {
            $allRoles = $this->roleService->loadRoles();
        } catch (UnauthorizedException $e) {
            $allRoles = [];
        }

        return $this->render($this->template, [
            'role' => $role,
            'parent_roles' => $parentRoles,
            'child_roles' => $childRoles,
            'route_name' => $routeName,
            'all_roles' => $allRoles,
            'can_update' => $this->isGranted(new Attribute('role', 'update')),
            'can_delete' => $this->isGranted(new Attribute('role', 'delete')),
            'can_create' => $this->isGranted(new Attribute('role', 'create')),
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $roleId = $this->handleRequest($request, true);
        return $this->redirectToRole($roleId);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request)
    {
        $roleId = $this->handleRequest($request, false);
        return $this->redirectToRole($roleId);
    }

    /**
     * @param $roleId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function redirectToRole($roleId)
    {
        return $this->redirect(
            $this->generateUrl('ezplatform.role.view', ['roleId' => $roleId]) . '#inheritance#tab'
        );
    }

    /**
     * @param Request $request
     * @param $create
     * @return mixed
     */
    protected function handleRequest(Request $request, $create)
    {
        $roleId = $request->request->get('roleId');
        $parentRoleIds = $request->request->get('parentRoleIds', []);
        $childRoleIds = $request->request->get('childRoleIds', []);

        foreach ($parentRoleIds as $parentRoleId) {
            foreach ($childRoleIds as $childRoleId) {
                if ($create) {
                    $this->roleInheritanceService->createRoleInheritance($parentRoleId, $childRoleId);
                }
                else {
                    $this->roleInheritanceService->deleteRoleInheritance($parentRoleId, $childRoleId);
                }
            }
        }

        return $roleId;
    }
}
