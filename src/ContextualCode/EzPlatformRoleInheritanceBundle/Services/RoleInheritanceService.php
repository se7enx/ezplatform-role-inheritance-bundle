<?php

namespace ContextualCode\EzPlatformRoleInheritanceBundle\Services;

use ContextualCode\EzPlatformRoleInheritanceBundle\Entity\RoleInheritance;
use ContextualCode\EzPlatformRoleInheritanceBundle\Entity\UserRoleInheritance;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use eZ\Publish\API\Repository\Repository;
use eZ\Publish\API\Repository\RoleService;
use eZ\Publish\API\Repository\UserService;
use eZ\Publish\API\Repository\Values\User\Role;
use eZ\Publish\API\Repository\Values\User\User;

class RoleInheritanceService
{
    /**
     * @var Repository
     */
    protected $repository;

    /**
     * @var RoleService
     */
    protected $roleService;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var EntityRepository
     */
    protected $roleInheritanceRepository;

    /**
     * @var EntityRepository
     */
    protected $userRoleInheritanceRepository;

    /**
     * RoleInheritanceService constructor.
     * @param Repository $repository
     * @param RoleService $roleService
     * @param UserService $userService
     * @param EntityManager $entityManager
     */
    public function __construct(
        Repository $repository,
        RoleService $roleService,
        UserService $userService,
        EntityManager $entityManager
    ) {
        $this->repository = $repository;
        $this->roleService = $roleService;
        $this->userService = $userService;
        $this->entityManager = $entityManager;
        $this->roleInheritanceRepository = $entityManager->getRepository(RoleInheritance::class);
        $this->userRoleInheritanceRepository = $entityManager->getRepository(UserRoleInheritance::class);
    }

    /**
     * @param $functionName
     * @param $args
     * @return mixed
     * @throws \Exception
     */
    protected function sudoize($functionName, $args) {
        return $this->repository->sudo(
            function ($repository) use ($functionName, $args) {
                return call_user_func_array([$this, $functionName], $args);
            }
        );
    }

    /**
     * @param $user
     * @return \eZ\Publish\API\Repository\Values\User\UserGroupRoleAssignment[]|\eZ\Publish\API\Repository\Values\User\UserRoleAssignment[]
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    protected function getRoleAssignmentsForUser($user)
    {
        return $this->roleService->getRoleAssignmentsForUser($user);
    }

    /**
     * @param $role
     * @param $user
     * @return mixed
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException
     * @throws \eZ\Publish\API\Repository\Exceptions\LimitationValidationException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    protected function assignUserToRole($role, $user)
    {
        return $this->roleService->assignRoleToUser($role, $user);
    }

    /**
     * @param $roleId
     * @return Role
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    protected function loadRole($roleId)
    {
        return $this->roleService->loadRole($roleId);
    }

    /**
     * @param $roleAssignmentId
     * @return \eZ\Publish\API\Repository\Values\User\RoleAssignment
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    protected function loadRoleAssignment($roleAssignmentId)
    {
        return $this->roleService->loadRoleAssignment($roleAssignmentId);
    }

    /**
     * @param $roleAssignment
     * @return mixed
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    protected function removeRoleAssignment($roleAssignment)
    {
        return $this->roleService->removeRoleAssignment($roleAssignment);
    }


    /**
     * @param $parentRoleId
     * @param $childRoleId
     * @return bool
     */
    public function createRoleInheritance($parentRoleId, $childRoleId)
    {
        $roleInheritance = new RoleInheritance();
        $roleInheritance->setParentRoleId(intval($parentRoleId));
        $roleInheritance->setChildRoleId(intval($childRoleId));

        try {
            $this->entityManager->persist($roleInheritance);
            $this->entityManager->flush();
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param $parentRoleId
     * @param $childRoleId
     * @return bool
     */
    public function deleteRoleInheritance($parentRoleId, $childRoleId)
    {
        $roleInheritance = $this->roleInheritanceRepository->findOneBy([
            'parentRoleId' => $parentRoleId,
            'childRoleId' => $childRoleId
        ]);

        if ($roleInheritance === null) {
            return false;
        }

        try {
            $this->entityManager->remove($roleInheritance);
            $this->entityManager->flush();
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param $params
     * @return array|object[]
     */
    public function fetch($params)
    {
        $result = $this->roleInheritanceRepository->findBy($params);
        return $result ? $result : [];
    }

    /**
     * @param $roleId
     * @return array|object[]
     */
    public function fetchAllParentRoleInheritances($roleId)
    {
        $parents = $this->roleInheritanceRepository->findBy([
            'childRoleId' => $roleId
        ]);

        return $parents ? $parents : [];
    }

    /**
     * @param $childRoleId
     * @return array
     */
    public function fetchAllParentRoles($childRoleId)
    {
        $parentRoles = [];
        $roleInheritances = $this->fetchAllParentRoleInheritances($childRoleId);
        foreach ($roleInheritances as $roleInheritance) {
            try {
                $parentRoleId = $roleInheritance->getParentRoleId();
                $parentRoles[$parentRoleId] = $this->roleService->loadRole($parentRoleId);
            } catch (\Exception $e) {
                continue;
            }
        }
        return $parentRoles;
    }

    /**
     * @param $parentRoleId
     * @return array|object[]
     */
    public function fetchAllChildRoleInheritances($parentRoleId)
    {
        $children = $this->roleInheritanceRepository->findBy([
            'parentRoleId' => $parentRoleId
        ]);

        return $children ? $children : [];
    }

    /**
     * @param $parentRoleId
     * @return array
     */
    public function fetchAllChildRoles($parentRoleId)
    {
        $childRoles = [];
        $roleInheritances = $this->fetchAllChildRoleInheritances($parentRoleId);
        foreach ($roleInheritances as $roleInheritance) {
            try {
                $childRoleId = $roleInheritance->getChildRoleId();
                $childRoles[$childRoleId] = $this->roleService->loadRole($childRoleId);
            } catch (\Exception $e) {
                continue;
            }
        }
        return $childRoles;
    }

    /**
     * @param $userId
     */
    public function handleUserChildRoles($userId)
    {
        try {
            $user = $this->userService->loadUser($userId);
        } catch (\Exception $e) {
            return;
        }
        if ($user instanceof User === false) {
            return;
        }

        $roleIdsToRoleAssignmentIds = $this->getRoleIdsToRoleAssignmentIdsFromUser($user);
        $roleIdsToAdd = $this->getRoleIdsToAdd($user);
        $this->addChildRoles($user, $roleIdsToAdd, $roleIdsToRoleAssignmentIds);
        $this->removeExpiredChildRoles($user, $roleIdsToAdd, $roleIdsToRoleAssignmentIds);
    }

    /**
     * @param User $user
     * @return array
     */
    protected function getRoleIdsToAdd(User $user)
    {
        $roleIdsToAdd = [];
        $roleIdsToRoleAssignmentIds = $this->getRoleIdsToRoleAssignmentIdsFromUser($user);
        foreach ($roleIdsToRoleAssignmentIds as $roleId => $roleAssignmentId) {
            $children = $this->fetchAllChildRoleInheritances($roleId);
            foreach ($children as $child) {
                $roleIdsToAdd[] = $child->getChildRoleId();
            }
        }
        $roleIdsToAdd = array_unique($roleIdsToAdd);

        return $roleIdsToAdd;
    }

    /**
     * @param $user
     * @return array
     */
    protected function getRoleIdsToRoleAssignmentIdsFromUser($user)
    {
        $roleAssignmentsToRoleIds = [];
        try {
            $roleAssignments = $this->sudoize('getRoleAssignmentsForUser', [$user]);
        } catch (\Exception $e) {
            $roleAssignments = [];
        }
        foreach ($roleAssignments as $roleAssignment) {
            $role = $roleAssignment->getRole();
            $roleAssignmentsToRoleIds[$role->id] = $roleAssignment->id;
        }
        return $roleAssignmentsToRoleIds;
    }

    /**
     * @param User $user
     * @param array $roleIdsToAdd
     * @param array $roleIdsToRoleAssignmentIds
     */
    protected function addChildRoles(User $user, array $roleIdsToAdd, array $roleIdsToRoleAssignmentIds)
    {
        $assignedRoleIds = array_keys($roleIdsToRoleAssignmentIds);
        foreach ($roleIdsToAdd as $roleIdToAdd) {
            if (in_array($roleIdToAdd, $assignedRoleIds)) {
                continue;
            }

            try {
                $role = $this->sudoize('loadRole', [$roleIdToAdd]);
            } catch (\Exception $e) {
                continue;
            }
            if ($role instanceof Role === false) {
                continue;
            }

            try {
                $this->sudoize('assignUserToRole', [$role, $user]);
            } catch (\Exception $e) {}

            $tmp = $this->userRoleInheritanceRepository->findBy([
                'userId' => $user->getUserId(),
                'childRoleId' => $role->id,
            ]);
            if (!$tmp || !is_array($tmp) || count($tmp) === 0) {
                $userRoleInheritance = new UserRoleInheritance();
                $userRoleInheritance->setUserId($user->getUserId());
                $userRoleInheritance->setChildRoleId($role->id);
                try {
                    $this->entityManager->persist($userRoleInheritance);
                    $this->entityManager->flush();
                } catch (\Exception $e) {
                    continue;
                }
            }
        }
    }

    /**
     * @param User $user
     * @param array $roleIdsToAdd
     * @param array $roleIdsToRoleAssignmentIds
     */
    protected function removeExpiredChildRoles(User $user, array $roleIdsToAdd, array $roleIdsToRoleAssignmentIds)
    {
        $existingUserRoleInheritances = $this->userRoleInheritanceRepository->findBy([
            'userId' => $user->getUserId()
        ]);
        if ($existingUserRoleInheritances === null) {
            return;
        }
        foreach ($existingUserRoleInheritances as $existingUserRoleInheritance) {
            $existingUserRoleInheritanceChildRoleId = $existingUserRoleInheritance->getChildRoleId();
            if (in_array($existingUserRoleInheritanceChildRoleId, $roleIdsToAdd)) {
                continue;
            }

            try {
                $role = $this->sudoize('loadRole', [$existingUserRoleInheritanceChildRoleId]);
            } catch (\Exception $e) {
                continue;
            }
            if ($role instanceof Role) {
                try {
                    $roleAssignmentId = $roleIdsToRoleAssignmentIds[$role->id];
                    $roleAssignment = $this->sudoize('loadRoleAssignment', [$roleAssignmentId]);
                    $this->sudoize('removeRoleAssignment', [$roleAssignment]);
                } catch (\Exception $e) {
                    continue;
                }
            }

            try {
                $this->entityManager->remove($existingUserRoleInheritance);
                $this->entityManager->flush();
            } catch (\Exception $e) {
                continue;
            }
        }
    }

}