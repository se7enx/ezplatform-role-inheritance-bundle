DROP TABLE IF EXISTS `cc_role_inheritance`;
CREATE TABLE `cc_role_inheritance` (
  `parent_role_id` int(11) unsigned NOT NULL,
  `child_role_id` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`parent_role_id`, `child_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `cc_user_role_inheritance`;
CREATE TABLE `cc_user_role_inheritance` (
  `user_id` int(11) unsigned NOT NULL,
  `child_role_id` int(11) unsigned NOT NULL,
  PRIMARY KEY  (`user_id`, `child_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;