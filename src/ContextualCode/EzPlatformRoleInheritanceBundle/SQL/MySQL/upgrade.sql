ALTER TABLE cc_roles_inheritance RENAME cc_role_inheritance;
ALTER TABLE cc_role_inheritance
DROP PRIMARY KEY;
ALTER TABLE cc_role_inheritance
CHANGE COLUMN inherit_role_id child_role_id int(11) unsigned NOT NULL;
ALTER TABLE cc_role_inheritance
ADD PRIMARY KEY(parent_role_id, child_role_id);

ALTER TABLE cc_user_roles_inheritance RENAME cc_user_role_inheritance;
ALTER TABLE cc_user_role_inheritance
DROP PRIMARY KEY;
ALTER TABLE cc_user_role_inheritance
CHANGE COLUMN inherit_role_id child_role_id int(11) unsigned NOT NULL;
ALTER TABLE cc_user_role_inheritance
ADD PRIMARY KEY(user_id, child_role_id);